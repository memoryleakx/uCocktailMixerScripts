Build your own Ubuntu Based Distribution with the uCocktailMixer script.
====
<h3>Howto use this script</h3>
<img class="wp-image-26 size-full alignleft" src="https://picload.org/image/rglidaar/drink-glass-blue-177x300.png" alt="drink-glass-blue-177x300" width="177" height="300" />

<u><strong>Step 1 – Download the script and install required packages</strong></u>

Download the script directly from <a href="https://github.com/memoryleakx/uCocktailMixerScripts/archive/master.zip">Github</a>.

Or simply clone the repository:
<pre><code class="bash hljs">git clone https://github.com/memoryleakx/uCocktailMixerScripts.git</code></pre>
Make sure you have this packages installed on your build machine.
<pre><code class="bash hljs">sudo apt-get install squashfs-tools genisoimage debootstrap syslinux</code></pre>
&nbsp;

<u><strong>Step 2 – Add your packages and configurations to the script</strong></u>

Search for the function named installYourPackages in the script file. Here you can Do your Magic ;)
<h3>HowTo install new Packages ?</h3>
<pre><code class="bash hljs">chrootExec apt-get install --yes packagename</code></pre>
<h3></h3>
<h3>HowTo edit config files on the environment ?</h3>
<pre><code class="bash hljs">#this appends a Line To the /etc/fstab file
echo "#Cool new Command Line" | chrootExec tee -a '/etc/fstab'

#Or you delete the file and create a complete new config file
chrootExec rm -rf /etc/fstab
chrootExec touch /etc/fstab
echo "#Cool new Command Line" | chrootExec tee -a '/etc/fstab'
</code></pre>
<h3></h3>
<h3>HowTo copy files from the build machine to your environment ?</h3>
<pre><code class="bash hljs">sudo cp /etc/fstab $chrootPath'/etc/fstab'</code></pre>
